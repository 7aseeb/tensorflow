package org.nebula.wisture.prediction;

/**
 * Created by haseeb on 11/11/16.
 */
public enum Gesture {
    noise,
    swipe,
    pause,
    push,
    pull
}
