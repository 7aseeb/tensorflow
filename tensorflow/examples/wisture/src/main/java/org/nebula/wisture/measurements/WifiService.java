package org.nebula.wisture.measurements;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.regex.Pattern;

import org.nebula.wisture.MainActivity;
import org.nebula.wisture.util.Constants;

/**
 * .
 * Created by haseeb on 3/23/16.
 */
public class WifiService extends IntentService {

    private static long NANOS_IN_SECOND = 1000000000;
    private int BUFFER_SIZE = 2000;
    private RandomAccessFile wirelessFile;

    public WifiService() {
        super(WifiService.class.getName());
        try {
            wirelessFile = new RandomAccessFile("/proc/net/wireless", "r");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static void sleep(int milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        float[] values = new float[BUFFER_SIZE];
        long[] times = new long[BUFFER_SIZE];

        long windowSpacingNanos = NANOS_IN_SECOND;

//                Inter-| sta-|   Quality        |   Discarded packets               | Missed | WE
//                face | tus | link level noise |  nwid  crypt   frag  retry   misc | beacon | 22
//                p2p0: 0000    0     0     0        0      0      0      0      0        0
//                wlan0: 0000   49.  -61.  -256        0      0      0      7    751        0

        int bufferIndex = 0;
        long previousWindowStart = System.nanoTime();
        // while the app is still running
        while (!MainActivity.measurementStopped) {
            // do we need to pause
            if (!MainActivity.measurementOngoing) {
                sleep(100);
                bufferIndex = 0;
                previousWindowStart = System.nanoTime();
                continue;
            }

            // take a measurement
            try {
                String line = wirelessFile.readLine();
                if (line != null) {
                    Float rssi = parseRssi(line);
                    if (rssi != null) {
                        values[bufferIndex] = rssi;
                        times[bufferIndex] = System.nanoTime();
                        bufferIndex++;
                        wirelessFile.seek(0); // Go to start
                    }
                } else
                    wirelessFile.seek(0); // Go to start
            } catch (IOException ex) {
                ex.printStackTrace();
            }

            // Todo check for index out of bound
            // See if we have a complete window to send
            if (System.nanoTime() - previousWindowStart > windowSpacingNanos) {
                reportMeasurements(values, times, bufferIndex);
                bufferIndex = 0;
                previousWindowStart = System.nanoTime();
            }

            // sleep a bit
            sleep(1);
        }

        // report last measurement
        reportMeasurementEnd();
    }

    private static Float parseRssi(String str){
        if (str.contains("wlan")) {
            str = str.trim();
            String[] split = str.split("\\s+");
            String rssiStr = split[3];
            if (rssiStr.contains("."))
                return Float.valueOf(rssiStr);
        }
        return null;
    }
    private void reportMeasurementEnd() {
        Intent measurementsEndIntent = new Intent(Constants.MEASUREMENT_END_BROADCAST_ACTION);
        LocalBroadcastManager.getInstance(this).sendBroadcast(measurementsEndIntent);
    }

    private void reportMeasurements(float[] values, long[] times, int numberOfSamples) {
    /*
     * Creates a new Intent containing a Uri object
     * NEW_MEASUREMENTS_BROADCAST_ACTION is a custom Intent action
     */
        Intent newMeasurementsReportIntent =
                new Intent(Constants.NEW_MEASUREMENTS_BROADCAST_ACTION);
        newMeasurementsReportIntent.putExtra(Constants.MEASUREMENTS_VALUES, values);
        newMeasurementsReportIntent.putExtra(Constants.MEASUREMENTS_TIMES, times);
        newMeasurementsReportIntent.putExtra(Constants.NUMBER_OF_MEASUREMENTS, numberOfSamples);

        // Broadcasts the Intent to receivers in this app.
        LocalBroadcastManager.getInstance(this).sendBroadcast(newMeasurementsReportIntent);
    }
}

