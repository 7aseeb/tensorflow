package org.nebula.wisture.prediction.dtw;

import java.util.Arrays;
import java.util.Comparator;

import org.nebula.wisture.prediction.dtw.DistanceGauge;

/**
 * Created by haseeb on 4/30/16.
 */
public class Knn {
    private DistanceGauge distanceGauge;
    private float[][] trainData;
    private int[] labels;

    private int neighbors = 5;
    private int subsampleStep = 1;

    public Knn(DistanceGauge distanceGauge, int neighbors, int subsampleStep) {
        this.distanceGauge = distanceGauge;
        this.neighbors = neighbors;
        this.subsampleStep = subsampleStep;
    }

    public Knn(DistanceGauge distanceGauge) {
        this.distanceGauge = distanceGauge;
    }

    public void fit(float[][] trainData, int[] labels) {
        this.trainData = trainData;
        this.labels = labels;
    }

    private float[][] getDistanceMatrix(float[] series, float[][] seriesGroup) {
        float[][] distances = new float[seriesGroup.length][2];
        for (int i = 0; i < seriesGroup.length; i++) {
            distances[i][0] = distanceGauge.getDistance(series, seriesGroup[i]);
            distances[i][1] = labels[i];
        }

        Arrays.sort(distances, new Comparator<float[]>() {
            @Override
            public int compare(float[] lhs, float[] rhs) {
                if (lhs[0] < rhs[0])
                    return -1;
                else if (lhs[0] > rhs[0])
                    return 1;
                else
                    return 0;
            }
        });

        return distances;
    }

    public int predict(float[] series) {
        float[][] distances = getDistanceMatrix(series, trainData);
        // TODO calculate the mode
        return (int) distances[0][1];
    }
}
