package org.nebula.wisture.prediction.edges;

import org.nebula.wisture.util.ArrayParser;

public class EdgesMain {

    private final static String dataFolder = "/nest/school/thesis_workspace/source/winiff/analysis/data_exploration";
    private final static String pullTimeSeriesFile = dataFolder + "/" + "pull.txt";
    private final static String pushTimeSeriesFile = dataFolder + "/" + "push.txt";
    //  private final static String pullTimesFile = dataFolder + "/" + "pull_time.txt";
//private final static String pushTimesFile = dataFolder + "/" + "push_time.txt";


    public static void main(String[] args) {
        SignalEdgeClassifier classifier = new SignalEdgeClassifier();
        System.out.println("features from a pull");
        System.out.println("prediction : " +
                classifier.predict(
                        null, //ArrayParser.parse1DArray(pullTimesFile),
                        ArrayParser.parse1DArray(pullTimeSeriesFile)
                )
        );

        System.out.println("\nfeatures from a push");
        System.out.println("prediction : " +
                classifier.predict(
                        null, // ArrayParser.parse1DArray(pushTimesFile),
                        ArrayParser.parse1DArray(pushTimeSeriesFile)
                )
        );
    }
}
