package org.nebula.wisture.prediction;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.widget.TextView;

import org.apache.commons.collections4.queue.CircularFifoQueue;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.logging.Logger;

import org.nebula.wisture.MainActivity;
import org.nebula.wisture.R;
import org.nebula.wisture.messaging.MessageSender;
import org.nebula.wisture.persistence.MeasurementsPersistenceManager;
import org.nebula.wisture.prediction.lstm.LstmGesturePredictorFactory;
import org.nebula.wisture.util.Constants;

/**
 * Created by haseeb on 4/29/16.
 */
public class MeasurementsReceiver extends BroadcastReceiver {

    private final static Logger logger = Logger.getLogger(MeasurementsReceiver.class.getName());
    private static long NANOS_IN_SECOND = 1000000000;

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }

    private final int BUFFER_SIZE = 4000;
    int[] colors = {Color.RED, Color.GREEN, Color.YELLOW, Color.GRAY, Color.BLUE};
    int windowSizeSeconds = 4;
    long windowSizeNanos = windowSizeSeconds * NANOS_IN_SECOND;
    MeasurementsPersistenceManager persistenceManager;
    Map<String, Integer> predictions;
    private Queue<Float> valuesBuffer = new CircularFifoQueue<>(BUFFER_SIZE);
    private Queue<Long> timesBuffer = new CircularFifoQueue<>(BUFFER_SIZE);
    private Context activityContext;
    private GesturePredictor predictor;
    private MessageSender sender;

    private double predictionTimeSum = 0;

    public MeasurementsReceiver(Context context) {
        activityContext = context;
        persistenceManager = new MeasurementsPersistenceManager(context);
//        predictor = LstmGesturePredictorFactory.create(context);
        predictor = LstmGesturePredictorFactory.createRaw(context);
        //predictor = new SignalEdgeClassifier();

        if (MainActivity.publishTraffic) {
            try {
                sender = new MessageSender(
                        "192.168.2.138", //"192.168.1.79",
                        -1,
                        "hello"
                );
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        predictions = new HashMap<>();
        predictions.put("noise", 0);
        predictions.put("swipe", 0);
        predictions.put("push", 0);
        predictions.put("pull", 0);
    }

    private void updatePredictions(Gesture gesture) {
        switch (gesture) {
            case swipe:
                predictions.put("swipe", predictions.get("swipe") + 1);
                break;
            case push:
                predictions.put("push", predictions.get("push") + 1);
                break;
            case pull:
                predictions.put("pull", predictions.get("pull") + 1);
                break;
            case noise:
            default:
                predictions.put("noise", predictions.get("noise") + 1);
        }
        System.out.println("swipe : " + predictions.get("swipe"));
        System.out.println("push : " + predictions.get("push"));
        System.out.println("pull : " + predictions.get("pull"));
        System.out.println("noise : " + predictions.get("noise"));
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        detect(intent);
    }

    public void detect(Intent intent) {
        long start = System.nanoTime();
        // Fetch the new data from the intent
        float[] values = intent.getFloatArrayExtra(Constants.MEASUREMENTS_VALUES);
        long[] times = intent.getLongArrayExtra(Constants.MEASUREMENTS_TIMES);
        int numberOfSamples = intent.getIntExtra(Constants.NUMBER_OF_MEASUREMENTS, 0);

        // insert the new data into the data buffers
        long lastTimeInBuffer = -10;
        for (int i = 0; i < numberOfSamples; i++) {
            valuesBuffer.add(values[i]);
            timesBuffer.add(times[i]);
            lastTimeInBuffer = times[i];
        }

        // Extract window from the data buffer
        List<Float> windowValues = new LinkedList<>();
        List<Long> windowTimes = new LinkedList<>();
        Iterator<Float> valuesBufferIter = valuesBuffer.iterator();
        long windowStartTime = lastTimeInBuffer - windowSizeNanos;
        if (windowStartTime < 0)
            return;
        for (Long currentTime : timesBuffer) {
            Float currentValue = valuesBufferIter.next();
            if (currentTime >= windowStartTime && currentTime <= lastTimeInBuffer) {
                windowTimes.add(currentTime);
                windowValues.add(currentValue);
            }
        }

        if (MainActivity.publishTraffic) {
            try {
                sender.send(windowTimes, windowValues);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        Gesture gesture = predictor.predict(windowTimes, windowValues);

        // show the prediction
        ((TextView) ((Activity) activityContext).findViewById(R.id.prediction)).setText(gesture.toString());
        ((Activity) activityContext).findViewById(R.id.layout).setBackgroundColor(colors[gesture.ordinal()]);
//        windowTimes.add((long) gesture.ordinal());
//
//        // collecting data for analysis
////        windowValues.add((float) gestureId);
////        windowValues.add(hightestLogit);
//
//        //persistenceManager.saveMeasurements(windowValues);
//
//        predictionTimeSum += (System.nanoTime() - start) / 1000000;
//        System.out.println("Avg inference time : " +
//                predictionTimeSum / (
//                        predictions.get("noise") +
//                                predictions.get("swipe") +
//                                predictions.get("push") +
//                                predictions.get("pull")
//                ) + " ms");
//        updatePredictions(gesture);
    }
}
