package org.nebula.wisture.prediction.edges;

import org.nebula.wisture.prediction.Gesture;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by haseeb on 11/11/16.
 */
public class SimpleStateSeqClassifier implements StateSeqClassifier {
    private List<List<State>> gestureSequences;
    private List<Gesture> gestures;
    private Gesture previousGesture;

    public SimpleStateSeqClassifier() {
        initGestureSequences();
    }

    private void initGestureSequences() {
        gestureSequences = new ArrayList<List<State>>(
                Arrays.asList(
                        new ArrayList<>(Arrays.asList(State.increase, State.decrease, State.pause)),
                        new ArrayList<>(Arrays.asList(State.decrease, State.pause)),
                        new ArrayList<>(Arrays.asList(State.pause, State.decrease, State.pause)),
                        new ArrayList<>(Arrays.asList(State.pause, State.pause)),
                        new ArrayList<>(Arrays.asList(State.pause)),
                        new ArrayList<>(Arrays.asList(State.decrease, State.pause, State.increase)),
                        new ArrayList<>(Arrays.asList(State.pause, State.increase)),
                        new ArrayList<>(Arrays.asList(State.pause, State.increase, State.pause))
                )
        );
        gestures = new ArrayList<>(
                Arrays.asList(
                        Gesture.push,
                        Gesture.push,
                        Gesture.push,
                        Gesture.pause,
                        Gesture.pause,
                        Gesture.pull,
                        Gesture.pull,
                        Gesture.pull
                )
        );

        previousGesture = null;
    }

    @Override
    public Gesture predict(List<StateEntry> sequence) {
        //        states.forEach(stateEntry -> System.out.println(stateEntry.getState() + " " + stateEntry.getCount()));
        List<StateEntry> reducedSeq = reduce(sequence);
        for (StateEntry stateEntry : reducedSeq) {
            System.out.print(stateEntry.getState() + "(" + stateEntry.getCount() + ") ");
        }
        System.out.println();
        Gesture gesture = applyBias(getGestureIdFromStates(reducedSeq));
        if (previousGesture == null) {
            if (gesture.equals(Gesture.push))
                previousGesture = gesture;
        } else
            previousGesture = gesture;

        System.out.println("Previous : " + previousGesture + " || Current : " + gesture);
        return gesture;
    }

    private Gesture applyBias(Gesture predicted) {
        // First gesture should be push
        if (previousGesture == null)
            if (predicted.equals(Gesture.push))
                return predicted;
            else
                return Gesture.noise;

        // After Noise can only be Push
        if (previousGesture.equals(Gesture.noise))
            if (predicted.equals(Gesture.push))
                return predicted;
            else
                return Gesture.noise;

        // After Push can only be Pull or Pause
        if (previousGesture.equals(Gesture.push))
            if (predicted.equals(Gesture.pause) || predicted.equals(Gesture.pull))
                return predicted;
            else
                return Gesture.pause;

        // After Pull can only be Push or Noise
        if (previousGesture.equals(Gesture.pull))
            if (predicted.equals(Gesture.push) || predicted.equals(Gesture.noise))
                return predicted;
            else
                return Gesture.noise;

        // After Pause can only be Pause or Pull
        if (previousGesture.equals(Gesture.pause))
            if (predicted.equals(Gesture.pause) || predicted.equals(Gesture.pull))
                return predicted;
            else
                return Gesture.pause;

        return Gesture.noise;
    }

    private Gesture getGestureIdFromStates(List<StateEntry> states) {
        int matchingSeqId = getMatchingListId(
                gestureSequences,
                getStatesFromStateEnteries(states)
        );
        if (matchingSeqId == -1)
            return Gesture.noise;
        else
            return gestures.get(matchingSeqId);
    }

    private <T> int getMatchingListId(List<List<T>> listsList, List<T> list) {
        for (int i = 0; i < listsList.size(); i++)
            if (listsList.get(i).equals(list))
                return i;
        return -1;
    }

    private List<State> getStatesFromStateEnteries(List<StateEntry> stateEntries) {
        List<State> states = new ArrayList<>();
        for (StateEntry stateEntry : stateEntries)
            states.add(stateEntry.getState());
        return states;
    }

    private List<StateEntry> reduce(List<StateEntry> states) {
        List<StateEntry> statesReduced = new ArrayList<>();
        for (StateEntry stateEntry : states) {
            if (statesReduced.size() == 0)
                // always add the first state
                statesReduced.add(stateEntry);
            else {
                int previousId = statesReduced.size() - 1;
                State previousState = statesReduced.get(previousId).getState();
                // if from increasing(decreasing) to decreasing(increasing)
                if ((previousState.equals(State.increase) &&
                        stateEntry.getState().equals(State.decrease)) ||
                        (previousState.equals(State.increase) &&
                                stateEntry.getState().equals(State.decrease)) ||
                        previousState.equals(State.pause))
                    statesReduced.add(stateEntry);
            }
        }
        // if last state is pause, add it to the reduced states
        if (states.get(states.size() - 1).getState().equals(State.pause))
            statesReduced.add(states.get(states.size() - 1));
        return statesReduced;
    }
}
