package org.nebula.wisture.prediction.edges;

/**
 * Created by haseeb on 11/6/16.
 */
public enum State {
    pause,
    increase,
    decrease
}
