package org.nebula.wisture.prediction.lstm;

import org.apache.commons.collections4.queue.CircularFifoQueue;

import java.util.Arrays;
import java.util.Collection;
import java.util.Queue;

import org.nebula.wisture.prediction.Gesture;

import static org.nebula.wisture.prediction.Gesture.noise;
import static org.nebula.wisture.prediction.Gesture.pull;
import static org.nebula.wisture.prediction.Gesture.push;
import static org.nebula.wisture.prediction.Gesture.swipe;

/**
 * Created by haseeb on 12/17/16.
 */
public class PredictionBias {
    private Queue<Gesture> previousGestures;

    public PredictionBias(int historyLength) {
        previousGestures = new CircularFifoQueue<>(historyLength);
    }

    public Gesture apply(Gesture gesture) {
//        printHistory(gesture);
        if ((previousGestures.size() == 0 && !gesture.equals(pull)) || // If we just started and the gesture is not null, or
                (!anyIn(Arrays.asList(swipe, push, pull), previousGestures) && // all is noise, and
                        anyIn(Arrays.asList(gesture), Arrays.asList(push, swipe))) // its push  or swipe
                ) {
            previousGestures.add(gesture);
            return gesture;
        }

        switch (gesture) {
            case pull:
                if (anyIn(Arrays.asList(push), previousGestures)) {
                    previousGestures.add(pull);
                    return pull;
                }
                break;

            case push:
                if (!anyIn(Arrays.asList(swipe, pull), previousGestures)) {
                    previousGestures.add(push);
                    return push;
                }
                break;

            case swipe:
                if (!anyIn(Arrays.asList(push, pull), previousGestures)) {
                    previousGestures.add(swipe);
                    return swipe;
                }
                break;
        }

        previousGestures.add(noise);
        return noise;
    }

    private boolean anyIn(Collection<Gesture> query, Collection<Gesture> target) {
        for (Gesture item : query)
            if (target.contains(item))
                return true;
        return false;
    }

    private void printHistory(Gesture currentGesture) {
        for (Gesture previousGesture : previousGestures) {
            System.out.print(previousGesture + " , ");
        }
        System.out.println(" -> " + currentGesture);
    }
}
