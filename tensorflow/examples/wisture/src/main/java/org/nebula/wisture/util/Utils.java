package org.nebula.wisture.util;

import android.annotation.SuppressLint;

import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by haseeb on 4/30/16.
 */
public class Utils {
    public static void print2DArray(float[][] array) {
        int rows = array.length;
        int cols = array[0].length;
        StringBuilder arrayString = new StringBuilder();
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                arrayString.append(array[i][j] + " ");
            }
            arrayString.append("\n");
        }
        System.out.println(arrayString.toString());
    }

    @SuppressLint("DefaultLocale")
    public static void print1DArray(float[] array) {
        System.out.println("Array length : " + array.length);
        StringBuilder arrayString = new StringBuilder();
        for (int i = 0; i < array.length; i++) {
            arrayString.append(String.format("%.4f", array[i]) + " ");
        }
        System.out.println(arrayString.toString());
    }

    public static void print1DArrayToFile(float[] array, String fileName) {

        FileWriter writer = null;
        try {
            writer = new FileWriter(fileName);
            StringBuilder arrayString = new StringBuilder();
            for (int i = 0; i < array.length; i++) {
                arrayString.append(array[i] + " ");
            }
            writer.append(arrayString.toString() + "\n");
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
