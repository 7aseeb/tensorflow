package org.nebula.wisture.prediction.lstm;

import android.content.Context;

import java.util.List;

/**
 * Created by haseeb on 12/11/16.
 */

public class RawPreprocessor extends Preprocessor {
    private float varThreshold;
    public RawPreprocessor(int numberOfIntervals, int numberOfStatistics, String meanFile, String stdFile, float varThreshold, Context context) {
        super(numberOfIntervals, numberOfStatistics, meanFile, stdFile, context);
        this.varThreshold = varThreshold;
    }

    public float[] processRaw(List<Long> times, List<Float> origValues) {
        // check if there is a legitimate activity
        float mean = getMean(origValues);
        if(getVar(origValues, mean) < varThreshold)
            return null;

        // subtract values mean
        subtract(origValues, mean);

        // sub-sample
        //  - mean / std
        return subsample(origValues);
    }

    private float[] subsample(List<Float> series) {
        float[] res = new float[numberOfIntervals];
        int dist = series.size() / numberOfIntervals;
        for (int i = 0; i < numberOfIntervals; i++) {
            int sampleId = dist * i;
            res[i] = (series.get(sampleId)  - mean[i][0]) /std[i][0];
        }
        return res;
    }


    private float getMean(List<Float> series) {
        float sum = 0;
        for (Float item : series)
            sum += item;
        return sum / series.size();
    }

    private void subtract(List<Float> series, float value) {
        for (int i = 0; i < series.size(); i++)
            series.set(i, series.get(i) - value);
    }


    private float getVar(List<Float> timeseries, float mean) {
        float diffSum = 0;
        for (int i = 0; i < timeseries.size(); i++)
            diffSum += Math.pow(timeseries.get(i) - mean, 2);
        return diffSum/timeseries.size();
    }
}
