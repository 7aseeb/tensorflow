package org.nebula.wisture.prediction.lstm;

import android.content.Context;

import org.tensorflow.contrib.android.TensorFlowInferenceInterface;

import java.util.List;

import org.nebula.wisture.prediction.Gesture;
import org.nebula.wisture.prediction.GesturePredictor;

import static org.nebula.wisture.prediction.Gesture.noise;
import static org.nebula.wisture.prediction.Gesture.pull;
import static org.nebula.wisture.prediction.Gesture.push;
import static org.nebula.wisture.prediction.Gesture.swipe;

/**
 * Created by haseeb on 10/16/16.
 */

public class LstmRawGesturePredictor implements GesturePredictor {

    private String predictorName;
    private String[] outputNames = new String[]{"Softmax/prediction", "Softmax/logits"};
    private RawPreprocessor preprocessor;
    private TensorFlowInferenceInterface inferer;
    private int numberOfIntervals;
    private int numberOfStats;
    private int batchSize = 1;
    private int numberOfClasses;
    private float[] thresholds;
    private float varThreshold = 0.3f;
    private PredictionBias predictionBias;

    public LstmRawGesturePredictor(Context context,
                                   int numberOfIntervals,
                                   int numberOfStats,
                                   int numberOfClasses,
                                   String modelPath,
                                   String meanPath,
                                   String stdPath,
                                   String predictorName,
                                   float[] thresholds) {
        this.predictorName = predictorName;
        this.numberOfIntervals = numberOfIntervals;
        this.numberOfStats = numberOfStats;
        this.numberOfClasses = numberOfClasses;
        this.thresholds = thresholds;
        /* init the data preprocessor */
        preprocessor = new RawPreprocessor(numberOfIntervals, numberOfStats, meanPath, stdPath, varThreshold, context);
        // init the tensorflow session
        inferer = new TensorFlowInferenceInterface();
        if (inferer.initializeTensorFlow(context.getAssets(), modelPath) != 0)
            throw new RuntimeException("Failed to initialize the a tensorflow session with the model in : " + modelPath);
        System.out.println("!!! Successfully initialized a tensorflow session : " + inferer.toString());
        this.predictionBias = new PredictionBias(3);
    }

    public Gesture predict(List<Long> times, List<Float> values) {
        int gestureId = predictThreshold(times, values);

        switch (gestureId) {
            case -1:
                return applyBias(noise);
            case 0:
                return applyBias(swipe);
            case 1:
                return applyBias(push);
            case 2:
                return applyBias(pull);
        }
        throw new RuntimeException("Unknown gesture id");
    }

    private Gesture applyBias(Gesture gesture) {
        Gesture biasedGesture = predictionBias.apply(gesture);
//        System.out.println(biasedGesture);
        return biasedGesture;
    }

    private int predictThreshold(List<Long> times, List<Float> values) {
//        System.out.println(" __________________________________" + predictorName + " ______________________________________");
        // preprocess
        float[] summary = preprocessor.processRaw(times, values);
        if (summary == null)
            return -1;

        // pass the input
        inferer.fillNodeFloat("input_data", batchSize, numberOfIntervals, numberOfStats, 1, summary);


        // dropout to 1
        float[] dropout = {1};
//        inferer.fillNodeFloat("Drop_out_keep_prob", 1, 1, 1, 1, dropout);

        //predict
        float[] logits = new float[batchSize * numberOfClasses];
        int[] predictionOutpus = new int[batchSize];
        if (inferer.runInference(outputNames) == 0) {
            inferer.readNodeInt(outputNames[0], predictionOutpus);
            inferer.readNodeFloat(outputNames[1], logits);
            int gestureId = -1;
            float highest = -10000;
            for (int i = 0; i < logits.length; i++) {
//                System.out.print(logits[i] + " ");
                if (logits[i] > highest) {
                    highest = logits[i];
                    gestureId = i;
                }
            }
//            System.out.println();
//            if (gestureId == 0 || highest > 2f)
            if ( highest > 2f)
                return gestureId;
            else
                return -1;

        } else {
            System.err.println("Failed to run inference");
            return -1;
        }
    }

    private int predictId(List<Long> times, List<Float> values) {
        System.out.println("\npredict !!!!!");
        // preprocess
        float[] summary = preprocessor.processRaw(times, values);

        // pass the input
        inferer.fillNodeFloat("input_data", batchSize, numberOfIntervals, numberOfStats, 1, summary);

        // dropout to 1
        inferer.fillNodeFloat("Drop_out_keep_prob", 1, 1, 1, 1, new float[]{1.0f});

        //predict
        int[] outputs = new int[batchSize];
        if (inferer.runInference(outputNames) == 0) {
            inferer.readNodeInt(outputNames[0], outputs);
            System.out.println(">>>>>>>>>>>>>>>>> " + outputs[0]);
            return outputs[0];
        } else {
            System.err.println("Failed to run inference");
            return 0;
        }
    }
}