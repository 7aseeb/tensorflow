package org.nebula.wisture.prediction.lstm;

import android.content.Context;

/**
 * Created by haseeb on 11/12/16.
 */

public class LstmGesturePredictorFactory {
    private static final String MEAN_FILE = "mean.txt";
    private static final String STD_FILE = "std.txt";
//    private static final String[] MODEL_FILES = {
////            "file:///android_asset/wisture_swipe_vs_all_correct_full",
//            "file:///android_asset/wisture_push_vs_all_correct_full",
////            "file:///android_asset/wisture_pull_vs_all_correct_full"
//    };

    private static final String[] MODEL_FILES = {
//            "file:///android_asset/wisture_all_classes_full"
            "file:///android_asset/raw_signal_full_full"
    };

    //    private static final String[] MODEL_FILES = {
//            "file:///android_asset/wisture_push_vs_all_full",
//            "file:///android_asset/wisture_pull_vs_all_full"
//    };
//    private static String[] labels = {"noise", "swipe", "push", "pull"};
    private static String[] labels = {"swipe", "push", "pull"};
    private static String[] predictorsNames = {"all"};
    private static int numberOfSteps = 4;
    private static int numberOfStats = 5;
    private float[][] thresholds = {
//            {-1000, 4}, // swipe
            {-1000, 0}, // push
//            {-1000, 5}, // pull
    };

    public static LstmGesturePredictor create(Context context) {
        return new LstmGesturePredictor(
                context,
                numberOfSteps,
                numberOfStats,
                labels.length,
                MODEL_FILES[0],
                MEAN_FILE,
                STD_FILE,
                predictorsNames[0],
                null
        );
    }

    public static LstmRawGesturePredictor createRaw(Context context) {
        return new LstmRawGesturePredictor(
                context,
                50,
                1,
                labels.length,
                MODEL_FILES[0],
                MEAN_FILE,
                STD_FILE,
                "raw",
                null
        );
    }
}
