package org.nebula.wisture.prediction.lstm;

import android.content.Context;

import org.tensorflow.contrib.android.TensorFlowInferenceInterface;

import java.util.List;

import org.nebula.wisture.prediction.Gesture;
import org.nebula.wisture.prediction.GesturePredictor;

/**
 * Created by haseeb on 10/16/16.
 */

public class LstmGesturePredictor implements GesturePredictor {

    private String predictorName;
    private String[] outputNames = new String[]{"Softmax/prediction", "Softmax/logits"};
    private Preprocessor preprocessor;
    private TensorFlowInferenceInterface inferer;
    private int numberOfIntervals;
    private int numberOfStats;
    private int batchSize = 1;
    private int numberOfClasses;
    private float[] thresholds;

    public LstmGesturePredictor(Context context,
                                int numberOfIntervals,
                                int numberOfStats,
                                int numberOfClasses,
                                String modelPath,
                                String meanPath,
                                String stdPath,
                                String predictorName,
                                float[] thresholds) {
        this.predictorName = predictorName;
        this.numberOfIntervals = numberOfIntervals;
        this.numberOfStats = numberOfStats;
        this.numberOfClasses = numberOfClasses;
        this.thresholds = thresholds;
        /* init the data preprocessor */
        preprocessor = new Preprocessor(numberOfIntervals, numberOfStats, meanPath, stdPath, context);
        // init the tensorflow session
        inferer = new TensorFlowInferenceInterface();
        if (inferer.initializeTensorFlow(context.getAssets(), modelPath) != 0)
            throw new RuntimeException("Failed to initialize the a tensorflow session with the model in : " + modelPath);
        System.out.println("!!! Successfully initialized a tensorflow session : " + inferer.toString());
    }

    public Gesture predict(List<Long> times, List<Float> values) {
        // Predict the gesture from the window
//        float[] predictions = new float[gestureDetectors.length];
//        int gestureId = 0;
//        float hightestLogit = -100;
//        for (int i = 0; i < gestureDetectors.length; i++) {
//            predictions[i] = gestureDetectors[i].predictThreshold(windowTimes, windowValues);
//            if (predictions[i] == -10000) // noise
//            {
//            } else if (predictions[i] > hightestLogit) {
//                hightestLogit = predictions[i];
//                gestureId = i + 1;
//            }
//        }

        int gestureId = predictId(times, values);
//        switch (gestureId) {
//            case 0:
//                return Gesture.noise;
//            case 1:
//                return Gesture.swipe;
//            case 2:
//                return Gesture.push;
//            case 3:
//                return Gesture.pull;
//        }
        switch (gestureId) {
            case 0:
                return Gesture.swipe;
            case 1:
                return Gesture.push;
            case 2:
                return Gesture.pull;
        }
        throw new RuntimeException("Unknown gesture id");
    }

    private int predictId(List<Long> times, List<Float> values) {
        System.out.println("\npredict !!!!!");
        // preprocess
        float[] summary = preprocessor.process(times, values);

        // pass the input
        inferer.fillNodeFloat("input_data", batchSize, numberOfIntervals, numberOfStats, 1, summary);

        //predict
        int[] outputs = new int[batchSize];
        if (inferer.runInference(outputNames) == 0) {
            inferer.readNodeInt(outputNames[0], outputs);
            System.out.println(">>>>>>>>>>>>>>>>> " + outputs[0]);
            return outputs[0];
        } else {
            System.err.println("Failed to run inference");
            return 0;
        }
    }

    private float predictThreshold(List<Long> times, List<Float> values) {
        System.out.println(" __________________________________" + predictorName + " ______________________________________");
        // preprocess
        float[] summary = preprocessor.process(times, values);

        // pass the input
        inferer.fillNodeFloat("input_data", batchSize, numberOfIntervals, numberOfStats, 1, summary);

        //predict
        float[] outputs = new float[batchSize * numberOfClasses];
        int[] predictionOutpus = new int[batchSize];
        if (inferer.runInference(outputNames) == 0) {
            inferer.readNodeInt(outputNames[0], predictionOutpus);
            inferer.readNodeFloat(outputNames[1], outputs);
            System.out.println("prediciton : " + predictionOutpus[0]);
            int gestureId = -1;
            float highest = -10000;
            for (int i = 0; i < outputs.length; i++) {
                if (outputs[i] > highest && outputs[i] > thresholds[i]) {
                    highest = outputs[i];
                    gestureId = i;
                }
                System.out.println(i + ": " + outputs[i]);
            }
            System.out.println("logit prediction : " + gestureId);
            if (gestureId == 0)
                return -10000;
            else
                return outputs[gestureId];
        } else {
            System.err.println("Failed to run inference");
            return 0;
        }
    }
}