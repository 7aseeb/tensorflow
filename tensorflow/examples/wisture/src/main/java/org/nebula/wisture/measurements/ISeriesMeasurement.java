package org.nebula.wisture.measurements;

/**
 * Created by haseeb on 3/24/16.
 */
public interface ISeriesMeasurement extends Measurement {
    public long getStepValue();
}
