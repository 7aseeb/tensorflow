package org.nebula.wisture.measurements;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;

import org.nebula.wisture.MainActivity;
import org.nebula.wisture.util.Constants;

/**
 * .
 * Created by haseeb on 3/23/16.
 */
public class WifiMeasurementsService extends IntentService {

    private static long NANOS_IN_SECOND = 1000000000;
    private int BUFFER_SIZE = 2000;

    public WifiMeasurementsService() {
        super(WifiMeasurementsService.class.getName());
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            Process shell = Runtime.getRuntime().exec("sh");
            DataOutputStream shOutputStream = new DataOutputStream(shell.getOutputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(shell.getInputStream()));

            float[] values = new float[BUFFER_SIZE];
            long[] times = new long[BUFFER_SIZE];

            long windowSpacingNanos = 1 * NANOS_IN_SECOND;


            shOutputStream.writeBytes("while :; do cat /proc/net/wireless; done;\n");
            shOutputStream.flush();
//                Inter-| sta-|   Quality        |   Discarded packets               | Missed | WE
//                face | tus | link level noise |  nwid  crypt   frag  retry   misc | beacon | 22
//                p2p0: 0000    0     0     0        0      0      0      0      0        0
//                wlan0: 0000   49.  -61.  -256        0      0      0      7    751        0

            String line;
            int bufferIndex = 0;
            long previousWindowStart = System.nanoTime();
            while (!MainActivity.measurementStopped) {
                if (!MainActivity.measurementOngoing) {
                    sleep(100);
                    bufferIndex = 0;
                    previousWindowStart = System.nanoTime();
                    continue;
                }
                line = reader.readLine();
                if (line.contains("wlan0")) {
                    String[] splitLine = line.split("\\s+");
                    // Add the time and the wifi value
                    values[bufferIndex] = Float.valueOf(splitLine[4]); // parsing the rssi value
                    times[bufferIndex] = System.nanoTime();
                    bufferIndex++;
                    // Todo check for index out of bound
                    // See if we have a complete window
                    if (System.nanoTime() - previousWindowStart > windowSpacingNanos) {
                        reportMeasurements(values, times, bufferIndex);
                        bufferIndex = 0;
                        previousWindowStart = System.nanoTime();
                    }
                }
                sleep(1);

            }

            reportMeasurementEnd();
            shOutputStream.writeBytes("exit\n");
            shOutputStream.flush();
            shell.waitFor();
            reader.close();
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }


    private void reportMeasurementEnd() {
        Intent measurementsEndIntent = new Intent(Constants.MEASUREMENT_END_BROADCAST_ACTION);
        LocalBroadcastManager.getInstance(this).sendBroadcast(measurementsEndIntent);
    }

    private void reportMeasurements(float[] values, long[] times, int numberOfSamples) {
    /*
     * Creates a new Intent containing a Uri object
     * NEW_MEASUREMENTS_BROADCAST_ACTION is a custom Intent action
     */
        Intent newMeasurementsReportIntent =
                new Intent(Constants.NEW_MEASUREMENTS_BROADCAST_ACTION);
        newMeasurementsReportIntent.putExtra(Constants.MEASUREMENTS_VALUES, values);
        newMeasurementsReportIntent.putExtra(Constants.MEASUREMENTS_TIMES, times);
        newMeasurementsReportIntent.putExtra(Constants.NUMBER_OF_MEASUREMENTS, numberOfSamples);

        // Broadcasts the Intent to receivers in this app.
        LocalBroadcastManager.getInstance(this).sendBroadcast(newMeasurementsReportIntent);
    }

    private static void sleep(int milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

