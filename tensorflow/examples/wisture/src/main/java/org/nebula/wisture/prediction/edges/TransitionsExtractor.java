package org.nebula.wisture.prediction.edges;

import org.apache.commons.collections4.queue.CircularFifoQueue;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

/**
 * Created by haseeb on 11/6/16.
 */
public class TransitionsExtractor {
    private ConvolutionFilter filter;
    private Differentiator differtiator;
    private Queue<Float> windowsAvgs;
    private float threshold;

    public TransitionsExtractor(int avgNumWindows, float threshold, ConvolutionFilter filter) {
        this.windowsAvgs = new CircularFifoQueue<>(avgNumWindows);
        this.threshold = threshold;
        this.filter = filter;
        this.differtiator = new Differentiator();
    }

    public List<StateEntry> extract(List<Float> series, List<Long> times) {
        // subtract mean
        subtractSequenceMean(series);

        // threshold
        threshold(series);

        // smooth
        List<Float> smoothedSereis = filter.convolve(series);

        // differentiate
        List<Float> differentiatedSeq = differtiator.differentiate(smoothedSereis, null);

        // extract sequences
        return getSequence(sign(differentiatedSeq));
    }

    private void threshold(List<Float> sequence) {
        for (int i = 0; i < sequence.size(); i++) {
            if (Math.abs(sequence.get(i)) <= threshold)
                sequence.set(i, 0f);
        }
    }

    private void subtractSequenceMean(List<Float> sequence) {
        // window mean
        float total = 0;
        for (float item : sequence)
            total += item;
        windowsAvgs.add(total / sequence.size());

        // windows moving avg
        float movingAvg = getMovingAvg();

        // subtract the moving avg
        for (int i = 0; i < sequence.size(); i++) {
            sequence.set(i, sequence.get(i) - movingAvg);
        }
    }

    private float getMovingAvg() {
        float avg = 0;
        for (Float aFloat : windowsAvgs)
            avg += aFloat;
        return avg / windowsAvgs.size();
    }

    private List<Integer> sign(List<Float> series) {
        List<Integer> signs = new ArrayList<>();
        for (Float entry : series) {
            if (entry > 0)
                signs.add(1);
            else if (entry < 0)
                signs.add(-1);
            else
                signs.add(0);
        }
        return signs;
    }

    private List<StateEntry> getSequence(List<Integer> signs) {
        List<StateEntry> states = new ArrayList<>();
        if(signs.size() == 0)
            return states;
        StateEntry stateEntry = new StateEntry(getStateFromInt(signs.get(0)), 0);
        states.add(stateEntry);
        for (Integer sign : signs) {
            State currentState = getStateFromInt(sign);
            if (states.get(states.size() - 1).getState().equals(currentState))
                states.get(states.size() - 1).setCount(states.get(states.size() - 1).getCount() + 1);
            else
                states.add(new StateEntry(currentState, 1));
        }
        return states;
    }

    private State getStateFromInt(int i) {
        if (i == 1)
            return State.increase;
        else if (i == -1)
            return State.decrease;
        else
            return State.pause;
    }

}