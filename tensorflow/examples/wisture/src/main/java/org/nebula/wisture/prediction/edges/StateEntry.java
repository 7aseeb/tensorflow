package org.nebula.wisture.prediction.edges;

/**
 * Created by haseeb on 11/11/16.
 */
public class StateEntry {
    private State state;
    private int count;

    public StateEntry(State state, int count) {
        this.state = state;
        this.count = count;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
