package org.nebula.wisture.prediction.dtw;

import java.util.Arrays;

import org.nebula.wisture.prediction.dtw.DistanceGauge;

/**
 * Created by haseeb on 4/30/16.
 * Based on the python implementation on
 * http://nbviewer.jupyter.org/github/markdregan/K-Nearest-Neighbors-with-Dynamic-Time-Warping/blob/master/K_Nearest_Neighbor_Dynamic_Time_Warping.ipynb
 */
public class DtwDistanceGauge implements DistanceGauge {

    private int maxWrappingWindow;

    public DtwDistanceGauge(int maxWrappingWindow) {
        this.maxWrappingWindow = maxWrappingWindow;
    }

    @Override
    public float getDistance(float[] series1, float[] series2) {
        // initialize a cost array and fill it with the max integer value
        int M = series1.length; //(int) (series1.length * 0.75f);
        int N = series2.length; // (int) (series2.length * 0.75f);
        float[][] cost = new float[M][N];
        for (int i = 0; i < M; i++)
            Arrays.fill(cost[i], Integer.MAX_VALUE);


        // initialize the first row and column
        cost[0][0] = dist(series1[0], series2[0]);
        for (int i = 1; i < M; i++)
            cost[i][0] = cost[i - 1][0] + dist(series1[i], series2[0]);
        for (int i = 1; i < N; i++)
            cost[0][i] = cost[0][i - 1] + dist(series1[0], series2[i]);

        // fill the rest of the cost matrix within the window
        for (int i = 1; i < M; i++)
            for (int j = Math.max(1, i - maxWrappingWindow); j < Math.min(N, i + maxWrappingWindow); j++)
                cost[i][j] = Math.min(Math.min(cost[i - 1][j - 1], cost[i][j - 1]), cost[i - 1][j]) + dist(series1[i], series2[j]);

        return cost[M - 1][N - 1];
    }

    private float dist(float a, float b) {
        return Math.abs(a - b);
    }
}
