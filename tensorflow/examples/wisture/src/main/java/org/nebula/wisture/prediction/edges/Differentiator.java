package org.nebula.wisture.prediction.edges;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by haseeb on 11/6/16.
 */
public class Differentiator {

    public List<Float> differentiate(List<Float> series, List<Float> times) {
        List<Float> output = new LinkedList<>();
        for (int i = 1; i < series.size(); i++)
            output.add(series.get(i) - series.get(i - 1));
        return output;
    }
}
