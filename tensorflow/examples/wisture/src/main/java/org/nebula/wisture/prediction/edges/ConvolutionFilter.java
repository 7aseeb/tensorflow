package org.nebula.wisture.prediction.edges;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by haseeb on 11/6/16.
 */
public class ConvolutionFilter {
    private float[] window;

    public ConvolutionFilter(int windowSize) {
        initFlatWindow(windowSize);
    }

    void initFlatWindow(int windowSize) {
        window = new float[windowSize];
        float frac = 1.0f / windowSize;
        for (int i = 0; i < window.length; i++)
            window[i] = frac;
    }

    public List<Float> convolve(List<Float> series) {
        int n = series.size();
        int m = window.length;
        List<Float> output = new LinkedList<>();
        for (int i = 0; i < n - m; i++) {
            float sum = 0;
            for (int j = 0; j < window.length; j++)
                sum += window[j] * series.get(i + j);
            output.add(sum);
        }
        return output;
    }
}
