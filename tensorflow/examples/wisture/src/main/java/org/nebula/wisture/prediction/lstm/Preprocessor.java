package org.nebula.wisture.prediction.lstm;

import android.content.Context;

import java.util.List;

import org.nebula.wisture.util.ArrayParser;

/**
 * Takes a time series:
 * <ul>
 * <li> removes the series mean</li>
 * <li> split it into intervals</li>
 * <li> generate a summary of 5 statistics to each interval</li>
 * <li> subtract a supplied mean array from the summary array</li>
 * <li> then divide by the std </li>
 * </ul>
 * Created by haseeb on 10/8/16.
 */
public class Preprocessor {
    static final float STABILIZER = 0.0f;
    final int numberOfStatistics;
    int numberOfIntervals;
    float[][] mean;
    float[][] std;

    public Preprocessor(int numberOfIntervals, int numberOfStatistics, String meanFile, String stdFile, Context context) {
        this.numberOfIntervals = numberOfIntervals;
        this.numberOfStatistics = numberOfStatistics;
        this.mean = ArrayParser.parse2DArray2(meanFile, context);
        this.std = ArrayParser.parse2DArray2(stdFile, context);
    }

    public float[] process(List<Long> times, List<Float> origValues) {
        // subtract values mean
        float[] values = subtractSequenceMean(origValues);
        threshold(values);

        // Create the output holder
        float[][] processedSequence = new float[numberOfIntervals][numberOfStatistics];
        // Get the time range and interval size
        float timeRange = times.get(times.size() - 1) - times.get(0);
        float intervalSize = timeRange / numberOfIntervals;
        float[] intervalStarts = new float[numberOfIntervals];
        for (int i = 0; i < numberOfIntervals; i++) {
            intervalStarts[i] = times.get(0) + i * intervalSize;
        }
        // loop through the timeseries once
        int currentIntervalId = 0;
        int intervalSamples = 0;
        float intervalSum = 0;
        float intervalMax = -Float.MAX_VALUE;
        float intervalMin = Float.MAX_VALUE;
        //System.out.println("Interval(" + currentIntervalId + ") start : " + intervalStarts[currentIntervalId] + " time : " + times.get(0) + " next : " + intervalStarts[currentIntervalId + 1]);
        for (int i = 0; i < values.length; i++) {
            float currentTime = times.get(i);
            float currentItem = values[i];
            // Check if a new interval started
            if (currentIntervalId < numberOfIntervals - 1 && currentTime > intervalStarts[currentIntervalId + 1]) {
                // store previous interval stats
                processedSequence[currentIntervalId][SummaryStats.mean.ordinal()] = intervalSum / (intervalSamples + STABILIZER);
                processedSequence[currentIntervalId][SummaryStats.var.ordinal()] = getVar(values, i - intervalSamples, i - 1, processedSequence[currentIntervalId][SummaryStats.mean.ordinal()]);
                processedSequence[currentIntervalId][SummaryStats.max.ordinal()] = intervalMax;
                processedSequence[currentIntervalId][SummaryStats.min.ordinal()] = intervalMin;
                processedSequence[currentIntervalId][SummaryStats.slope.ordinal()] = getSlope(values, i - intervalSamples, i - 1);
                // reset stats for next interval
                currentIntervalId++;
                intervalSamples = 0;
                intervalSum = 0;
                intervalMax = -Float.MAX_VALUE;
                intervalMin = Float.MAX_VALUE;
                //System.out.println("Interval(" + currentIntervalId + ") start : " + intervalStarts[currentIntervalId] + " time : " + times.get(i));
            }
            // Mean calculations
            intervalSamples++;
            intervalSum += currentItem;
            // Max calculations
            if (currentItem > intervalMax) intervalMax = currentItem;
            // Min calculations
            if (currentItem < intervalMin) intervalMin = currentItem;
            // If it is the last loop
            if (i == values.length - 1) {
                processedSequence[currentIntervalId][SummaryStats.mean.ordinal()] = intervalSum / (intervalSamples + STABILIZER);
                processedSequence[currentIntervalId][SummaryStats.var.ordinal()] = getVar(values, i - intervalSamples + 1, i, processedSequence[currentIntervalId][SummaryStats.mean.ordinal()]);
                processedSequence[currentIntervalId][SummaryStats.max.ordinal()] = intervalMax;
                processedSequence[currentIntervalId][SummaryStats.min.ordinal()] = intervalMin;
                processedSequence[currentIntervalId][SummaryStats.slope.ordinal()] = getSlope(values, i - intervalSamples + 1, i);
            }
        }
        standardize(processedSequence);
        return convertTo1D(processedSequence);
    }

    float[] convertTo1D(float[][] arr) {
        float[] resArr = new float[arr.length * arr[0].length];
        int i = 0;
        for (float[] floats : arr)
            for (float aFloat : floats) {
                resArr[i] = aFloat;
                i++;
            }
        return resArr;
    }

    void standardize(float[][] array) {
        for (int i = 0; i < array.length; i++)
            for (int j = 0; j < array[0].length; j++)
                // subtract mean + divide by std
                array[i][j] = (array[i][j] - mean[i][j]) / std[i][j];
    }

    float[] subtractSequenceMean(List<Float> sequence) {
        float[] centeredSequence = new float[sequence.size()];
        float total = 0;
        for (float item : sequence)
            total += item;
        float mean = total / sequence.size();
        for (int i = 0; i < sequence.size(); i++)
            centeredSequence[i] = sequence.get(i) - mean;
        return centeredSequence;
    }

    void threshold(float[] sequence) {
        for (int i = 0; i < sequence.length; i++)
            if (Math.abs(sequence[i]) < 1)
                sequence[i] = 0;
    }

    private float getVar(float[] timeseries, int start, int end, float mean) {
        float diffSum = 0;
        for (int i = start; i <= end; i++) {
            diffSum += Math.pow(timeseries[i] - mean, 2);
        }
        return diffSum / (end - start + 1 + STABILIZER);
    }

    private float getSlope(float[] timeseries, int start, int end) {
        float slope = 0;
        for (int i = start + 1; i <= end; i++) {
            slope += timeseries[i] - timeseries[i - 1];
        }
        return slope;
    }

    private void print2DFloatArray(float[][] arr) {
        System.out.println("$$$$$$ ");
        for (float[] anArr : arr)
            for (float anAnArr : anArr)
                System.out.print(anAnArr + " ");
        System.out.println();
    }

    private enum SummaryStats {
        mean,
        var,
        max,
        min,
        slope
    }
}
