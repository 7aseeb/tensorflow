package org.nebula.wisture.messaging;

import android.os.AsyncTask;
import android.util.Log;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;

import java.io.IOException;

/**
 * Created by haseeb on 12/14/16.
 */

public class MessagePublisherTask extends AsyncTask<Object, Void, Void> {
    @Override
    protected Void doInBackground(Object... params) {
        Channel channel = (Channel) params[0];
        String msg = (String) params[1];
        String queue = (String) params[2];
        try {
            channel.basicPublish("", queue, null, msg.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
//        return params.length;
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        System.out.println("sent");
    }

}
