package org.nebula.wisture.messaging;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeoutException;

/**
 * Created by haseeb on 11/19/16.
 */
public class MessageSender {
    private Channel channel;
    private String queueName;

    public MessageSender(String host, int port, String queueName) throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(host);
        if (port != -1) factory.setPort(port);
        Connection connection = factory.newConnection();

        channel = connection.createChannel();
        channel.queueDeclare(queueName, false, false, false, null);

        this.queueName = queueName;


    }

    public void send(List<Long> windowTimes, List<Float> windowValues) throws IOException {
        String message = getString(windowTimes) + "|" + getString(windowValues);
        MessagePublisherTask sendingTask = new MessagePublisherTask();
        sendingTask.execute(channel, message, queueName);
    }

    private <T extends Number> String getString(List<T> list) {
        String string = "";
        for (T t : list) {
            string += t.toString() + ",";
        }
        return string.substring(0, string.length() - 1);
    }
}
