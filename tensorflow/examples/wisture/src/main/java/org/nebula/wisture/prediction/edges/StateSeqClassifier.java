package org.nebula.wisture.prediction.edges;

import java.util.List;

import org.nebula.wisture.prediction.Gesture;

/**
 * Created by haseeb on 11/11/16.
 */
public interface StateSeqClassifier {
    public Gesture predict(List<StateEntry> states);
}
