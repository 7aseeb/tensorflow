package org.nebula.wisture.prediction.edges;

import org.nebula.wisture.prediction.Gesture;
import org.nebula.wisture.prediction.GesturePredictor;

import java.util.List;

/**
 * Created by haseeb on 11/10/16.
 */
public class SignalEdgeClassifier implements GesturePredictor {
    private TransitionsExtractor transitionsExtractor;
    private StateSeqClassifier seqClassifier;

    public SignalEdgeClassifier() {
        this.transitionsExtractor = new TransitionsExtractor(
                2, // moving avg over 2 consecutive windows
                1.5f, // set less amplitudes to 0
                new ConvolutionFilter(91) // use a filter of 91 measurements
        );
        this.seqClassifier = new SimpleStateSeqClassifier();
    }

    @Override
    public Gesture predict(List<Long> times, List<Float> values) {
        return seqClassifier.predict(
                transitionsExtractor.extract(values, times)
        );
    }
}
