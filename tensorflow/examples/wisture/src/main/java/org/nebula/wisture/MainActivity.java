package org.nebula.wisture;

import android.content.Intent;
import android.content.IntentFilter;
import android.net.DhcpInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.text.format.Formatter;
import android.view.View;
import android.widget.Button;

import org.nebula.wisture.measurements.WifiService;
import org.nebula.wisture.prediction.MeasurementsReceiver;
import org.nebula.wisture.traffic.TrafficInductionService;
import org.nebula.wisture.util.Constants;

public class MainActivity extends AppCompatActivity {

    public static boolean induceTraffic = true;
    public static boolean publishTraffic = false;
    public static boolean measurementOngoing = false;
    public static boolean measurementStopped = false;

    private MeasurementsReceiver receiver;
    private Button ctrButton;
    private Button indButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // below is to be able to do TCP from the main thread; needed for the rabbitMq stuff
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        initCtrButton();
    }

    @Override
    protected void onStart() {
        super.onStart();
        registerMeasurementsReceiver();
        startMeasurementsService();
//        if (induceTraffic) startTrafficInductionService();
        startTrafficInductionService();
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterMeasurementsReceiver();
        stopMeasurementsService();
//        if (induceTraffic) stopTrafficInductionService();
        stopTrafficInductionService();
    }

    private void initCtrButton() {
        ctrButton = (Button) findViewById(R.id.ctrl);
        ctrButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!measurementOngoing) {
                    measurementOngoing = true;
                    ctrButton.setText(R.string.stop);
                } else {
                    measurementOngoing = false;
                    ctrButton.setText(R.string.start);
                }
            }
        });

        indButton = (Button) findViewById(R.id.induction);
        indButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!induceTraffic) {
                    induceTraffic = true;
                    indButton.setText(R.string.disable_induction);
                } else {
                    induceTraffic = false;
                    indButton.setText(R.string.enable_induction);
                }
            }
        });
    }

    /***
     * Measurements service
     ***/
    private void registerMeasurementsReceiver() {
        receiver = new MeasurementsReceiver(this);
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter(Constants.NEW_MEASUREMENTS_BROADCAST_ACTION));
    }

    private void unregisterMeasurementsReceiver() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }

    private void startMeasurementsService() {
        this.startService(new Intent(this, WifiService.class));
    }

    private void stopMeasurementsService() {
        this.stopService(new Intent(this, WifiService.class));
    }

    /***
     * Traffic induction service
     ***/
    private void startTrafficInductionService() {
        Intent induction = new Intent(this, TrafficInductionService.class);
        String gatewayAddress = getGatewayAddress();
        induction.putExtra("gatewayAddress", gatewayAddress);
        this.startService(induction);
    }

    private String getGatewayAddress(){
        WifiManager manager = (WifiManager) this.getApplicationContext().getSystemService(WIFI_SERVICE);
        DhcpInfo info = manager.getDhcpInfo();
        return Formatter.formatIpAddress(info.gateway);
    }

    private void stopTrafficInductionService() {
        this.stopService(new Intent(this, TrafficInductionService.class));
    }
}
