package org.nebula.wisture.prediction.dtw;

/**
 * Created by haseeb on 4/30/16.
 */
public interface DistanceGauge {
    float getDistance(float[] series1, float[] series2);
}
