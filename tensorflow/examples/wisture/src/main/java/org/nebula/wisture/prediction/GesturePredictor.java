package org.nebula.wisture.prediction;

import java.util.List;

/**
 * Created by haseeb on 11/12/16.
 */

public interface GesturePredictor {
    public Gesture predict(List<Long> times, List<Float> values);
}
