package org.nebula.wisture.util;

import android.content.Context;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by haseeb on 4/30/16.
 */
public class ArrayParser {

    public static float[][] parse2DArray(String fileName, Context context) {
        InputStream inStream = null;
        try {
            inStream = context.getAssets().open(fileName);
            return parse2DArray(inStream);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static float[][] parse2DArray2(String fileName, Context context) {
        InputStream fileStream = null;
        try {
            fileStream = context.getAssets().open(fileName);
            ArrayList<Float> floatList = new ArrayList<>();
            BufferedReader reader = new BufferedReader(new InputStreamReader(fileStream));
            String line;
            while ((line = reader.readLine()) != null)
                floatList.add(Float.parseFloat(line));
            float[][] resultArray = new float[floatList.size()][1];
            for (int i = 0; i < floatList.size(); i++)
                resultArray[i][0] = floatList.get(i);
            return resultArray;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static float[][] parse2DArray(InputStream fileStream) {
        ArrayList<float[]> arrays = new ArrayList<>();
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(fileStream));
            String line;
            int lineNum = 0;
            while ((line = reader.readLine()) != null) {
                String[] splitLine = line.split("\\s+");
                if (splitLine.length < 2)
                    throw new RuntimeException("More than one number per line is expected !!!!");
                arrays.add(new float[splitLine.length]);
                int tokenNum = 0;
                for (String token : splitLine) {
                    arrays.get(lineNum)[tokenNum] = Float.parseFloat(token);
                    tokenNum++;
                }
                lineNum++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        float[][] resultArray = new float[arrays.size()][];
        for (int i = 0; i < arrays.size(); i++)
            resultArray[i] = arrays.get(i);
        return resultArray;
    }

    public static List<Float> parse1DArray(String arrayFile) {
        try {
            return parse1DArrayFloat(new FileInputStream(new File(arrayFile)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static List<Float> parse1DArrayFloat(InputStream fileStream) {
        ArrayList<Float> floatList = new ArrayList<>();
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(fileStream));
            String line;
            while ((line = reader.readLine()) != null) {
                floatList.add(Float.parseFloat(line));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return floatList;
    }

    public static int[] parse1DArray(InputStream fileStream) {
        ArrayList<Integer> intsList = new ArrayList<>();
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(fileStream));
            String line;
            while ((line = reader.readLine()) != null) {
                String[] splitLine = line.split("\\s+");
                if (splitLine.length > 1)
                    throw new RuntimeException("Found more than multiples numbers in single line!!!!");
                intsList.add(Integer.parseInt(splitLine[0]));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        int[] array = new int[intsList.size()];
        for (int i = 0; i < intsList.size(); i++) {
            array[i] = intsList.get(i);
        }
        return array;
    }

}
