This repository contains the source of Wisture, the hand gesture recognition Android application, proposed and developed by [this thesis](http://www.nada.kth.se/~ann/exjobb/mohamed_abdulaziz-ali-haseeb.pdf). The model used by the application is trained with Tensorflow, and for reasons related to how such models are embeded in Android applications, the Wisture application source, is based on a fork of the tensorflow repo.

To compile and install the application follow below steps (some of these steps assume basic knowledge of building Android applications):

* clone this repository
* Install bazel and Android NDK following the instructions described [here](https://github.com/tensorflow/tensorflow/tree/master/tensorflow/examples/android/)
* Install Android studio
* Train the LSTM RNN model by following the instructions described [here](https://gits-15.sys.kth.se/moaah/winiff)
* Copy the trained model graph, the training mean and std files (created in above step), to tensorflow/examples/wisture/assets folder under your clone of this repo
* From Android studio, open the wisture Android application source located in tensorflow/examples/wisture under your clone of this repo
* Compile and build the application. The output is an apk file that can be installed directly to your Android device or uploaded to Google play!

I am sure these instructions are far from complete :) so if you need help contact me on mohamedaziz52@gmail.com